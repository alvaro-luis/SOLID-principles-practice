﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public Animator playerAnimator;

    public PlayerMovement playerMovement;
    public PlayerRenderer playerRenderer;
    public PlayerAIInteractions playerAIInteractions;
    public IMovementInput movementInput;
    public PlayerAnimations playerAnimations;

    public UiController uiController;
    
    public float movementSpeed;

    //public GameObject ui_window;

    //private Rigidbody2D rb2d; ya no debe estar

    private Vector2 movementVector;

    private void Start()
    {
        playerAnimations = GetComponent<PlayerAnimations>();
        playerMovement = GetComponent<PlayerMovement>();
        playerRenderer = GetComponent<PlayerRenderer>();
        playerAIInteractions = GetComponent<PlayerAIInteractions>();
		movementInput = GetComponent<IMovementInput>();
        movementInput.OnInteractEvent += () => playerAIInteractions.Interact(playerRenderer.MySpriteFlipped);
    }

    private void FixedUpdate()
    {
        //MovePlayer(playerInput.MovementInputVector);
        playerMovement.MovePlayer(movementInput.MovementInputVector);
        playerRenderer.RendererPlayer(movementInput.MovementInputVector);
        playerAnimations.SetUpAnimations(movementInput.MovementInputVector);
        if (movementInput.MovementInputVector.magnitude > 0)
        {
            //ui_window.SetActive(false);
            uiController.ToggleUI(false);
        }
    }

    //private void MovePlayer(Vector2 movementVector)
    //{
    //    playerAnimator.SetBool("Walk", true);
    //    playerMovement.MovePlayer(movementVector);

    //    playerRenderer.RendererPlayer(movementVector);
    //}

    public void ReceiveDamaged()
    {
        playerRenderer.FlashRed();
    }
}
