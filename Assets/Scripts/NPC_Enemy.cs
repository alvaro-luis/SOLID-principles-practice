﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class NPC_Enemy : NPC
{
    //public GameObject ui_window;
    //public AudioSource audioSource;
    //public Text textField;
    //public string text = "I deal 10 physical damage    ( •̀ᴗ•́ )و ̑̑ ";

    public string text = "I deal 10 physical damage    ( •̀ᴗ•́ )و ̑̑ ";


    //public void GetHit()
    //{
    //    //ui_window.SetActive(true);
    //    //textField.text = text;
    //    //audioSource.Play();
    //    //FindObjectOfType<Player>().ReceiveDamaged();  
    //}
    /*Ya no existe y se reemplaza por el Interact() de abajo*/

    public override void Interact()
    {
        base.Interact();
        FindObjectOfType<Player>().ReceiveDamaged();
    }

    protected override string GetText()
    {
        return text;
    }
}
