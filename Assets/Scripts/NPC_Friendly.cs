﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class NPC_Friendly : NPC
{
    //public GameObject ui_window;
    //public UnityEvent<string> OnSpeak;
    //public AudioSource audioSource;
    //public Text textField;
    public string text = "Hi there. Look out for that KOBOLD on the other side!";

    //public void Talk()
    //{
    //    //ui_window.SetActive(true);
    //    //textField.text = text;
    //    OnSpeak?.Invoke(text);
    //    audioSource.Play();
    //}

    protected override string GetText()
    {
        return text;
    }
}
