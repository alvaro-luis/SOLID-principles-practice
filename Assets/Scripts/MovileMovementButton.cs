using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MovileMovementButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public PlayerMobileInput input;
    public Vector2 movementDirection;

    public void OnPointerDown(PointerEventData eventData)
    {
        input.GetMovementInput(movementDirection);
        Debug.Log(movementDirection);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        input.ResetInput();
        //Debug.Log(movementDirection);
    }
}
